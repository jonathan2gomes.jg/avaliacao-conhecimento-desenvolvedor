
# Sistema de Gestão de Funcionários

O Sistema de Gestão de Funcionários é um aplicativo Gradle que fornece APIs para gerenciar dados de funcionários. Ele permite que os usuários realizem várias operações, como criar, atualizar, recuperar e excluir registros de funcionários.

## Sumário

- [Tecnologias](#tecnologias)
- [Instalação](#instalação)
- [Uso](#uso)
- [Endpoints da API](#endpoints-da-api)


## Tecnologias

O Sistema de Gestão de Funcionários é construído com as seguintes tecnologias:

- Java 17
- Spring Boot 3.0.0
- Gradle

## Instalação

1. Clone o repositório:

```bash
git clone https://gitlab.com/jonathan2gomes.jg/avaliacao-conhecimento-desenvolvedor.git
```

2. Navegue até o diretório do projeto:

```bash
cd sistema-gestao-funcionarios
````

3. Build o aplicativo:

```bash
gradlew build
```

4. Suba o banco de dados utilizando o comando:
```bash
docker-compose up -d
```

5. Execute o aplicativo:
```bash
gradlew bootRun
```

## Uso

O Sistema de Gestão de Funcionários fornece uma API RESTful para gerenciar dados de funcionários. Você pode usar ferramentas como cURL ou Postman para interagir com os endpoints da API.

Para a aplicação funcionar corretamente, é necessário que a máquina tenha Docker instalado.

Certifique-se de que o aplicativo esteja sendo executado localmente seguindo as etapas de instalação. Em seguida, você pode enviar solicitações HTTP para os endpoints da API.

## Endpoints da API

### swagger-ui

Como padrão, a aplicação é executada na porta 8080.

A documentação da API pode ser acessada através do Swagger-UI.

**Endpoint:** `http://localhost:8080/swagger-ui.html`

### Criar Colaborador

**Endpoint:** `POST /colaborador`

Cria um novo Colaborador.

### IMPORTANTE
**O primeiro colaborador a ser criado deve ser o Presidente, com o campo idGerente nulo, para que seja possível, a partir deste ponto, criar os demais colaboradores e a hierarquia de gerentes.**

**Corpo da Requisição:**

```json
{
"nome": "Joliaber",
"cpf": "123456789",
"dataAdmissao": "2023-05-01",
"funcao": "Presidente",
"remuneracao": 50000.00,
"idGerente": null
}
```
### Excluir Colaborador
**Endpoint:** ` DELETE /colaborador/{id}`

Exclui um Colaborador pelo ID.

### Obter Colaborador por CPF
**Endpoint:** `GET /colaborador/cpf/{cpf}`

Recupera um Colaborador pelo CPF.

Obter Colaborador por ID
**Endpoint:** ` GET /colaborador/id/{id}`

Recupera um Colaborador pelo ID.

Obter Todos os Colaboradores
**Endpoint:** ` GET /colaborador`

Recupera todos os Colaboradores.

Obter Colaboradores por Ano
**Endpoint:** ` GET /colaborador/ano/{ano}`

Recupera todos os Colaboradores admitidos no ano especificado.

Obter Gerente Comum
**Endpoint:** ` GET /colaborador/responsavel?id={id}&id2={id2}`

Recupera o gerente comum responsável pelos Colaboradores fornecidos.

Atualizar Colaborador
**Endpoint:** ` PUT /colaborador/{id}`

### Atualiza os dados de um Colaborador.

Corpo da Requisição:

```json
{
  "nome": "Agbaldo",
  "cpf": "987654321",
  "dataAdmissao": "2023-05-01",
  "funcao": "Gerente",
  "remuneracao": 7000.00,
  "idGerente": 1
}
```








