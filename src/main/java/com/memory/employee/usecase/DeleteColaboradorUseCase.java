package com.memory.employee.usecase;

import com.memory.employee.entity.colaborador.gateway.ColaboradorGateway;
import com.memory.employee.entity.colaborador.model.Colaborador;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeleteColaboradorUseCase {

    private final ColaboradorGateway colaboradorGateway;

    public DeleteColaboradorUseCase(ColaboradorGateway colaboradorGateway) {
        this.colaboradorGateway = colaboradorGateway;
    }

    public void execute(Input input) {

        List<Long> idsSubordinados = colaboradorGateway.findById(input.id).getIdSubordinados();

        if (colaboradorGateway.findById(input.id).getFuncao().equalsIgnoreCase("presidente")) {
            throw new RuntimeException("Não é possível deletar o presidente");
        }

        if (idsSubordinados != null) {

            for (Long id : idsSubordinados) {
                Colaborador colaboradorToUpdateGerente = colaboradorGateway.findById(id);
                colaboradorToUpdateGerente.setIdGerente(colaboradorGateway.findById(input.id).getIdGerente());
                colaboradorGateway.update(colaboradorToUpdateGerente, id);
            }
        }
        colaboradorGateway.delete(input.id);
    }


    public record Input(
            Long id
    ) {}
}
