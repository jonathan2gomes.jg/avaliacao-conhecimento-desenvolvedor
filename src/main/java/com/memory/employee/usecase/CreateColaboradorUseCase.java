package com.memory.employee.usecase;

import com.memory.employee.entity.colaborador.gateway.ColaboradorGateway;
import com.memory.employee.entity.colaborador.model.Colaborador;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class CreateColaboradorUseCase {

    private final ColaboradorGateway colaboradorGateway;

    public CreateColaboradorUseCase(ColaboradorGateway colaboradorGateway) {
        this.colaboradorGateway = colaboradorGateway;
    }

    public void execute(Input input) {

        validateColaborador(input);

        var colaborador = new Colaborador(
            null,
            input.nome,
            input.cpf,
            input.dataAdmissao(),
            input.funcao(),
            input.remuneracao(),
            input.idGerente(),
            null);

        colaboradorGateway.create(colaborador);
    }

    private void validateColaborador(Input input) {
        if (colaboradorGateway.findByCpfOptional(input.cpf).isPresent()) {
            throw new RuntimeException("Já existe um colaborador com o cpf " + input.cpf);
        }

        validateGerente(input.funcao, input.idGerente);
        validateFuncao(input.funcao);
    }

    private void validateGerente(String funcao, Long idGerente) {
        if (!funcao.equalsIgnoreCase("presidente") && idGerente == null) {
            throw new RuntimeException("O campo idGerente não pode ser nulo");
        }

        if (idGerente != null && colaboradorGateway.findById(idGerente) == null) {
            throw new RuntimeException("O Gerente de id " + idGerente + " não existe");
        }
    }
    private void validateFuncao(String funcao) {
        if (funcao.equalsIgnoreCase("presidente")) {
            colaboradorGateway.findAll().stream()
                    .filter(colaborador -> colaborador.getFuncao().equalsIgnoreCase("presidente"))
                    .findFirst()
                    .ifPresent(colaborador -> {
                        throw new RuntimeException("Já existe um presidente, por favor cadastre um subordinado");
                    });
        }
    }


    public record Input(
        String nome,
        String cpf,
        LocalDate dataAdmissao,
        String funcao,
        Double remuneracao,
        Long idGerente

    ) {}
}
