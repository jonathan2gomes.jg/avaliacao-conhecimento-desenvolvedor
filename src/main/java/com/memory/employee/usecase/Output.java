package com.memory.employee.usecase;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;
import java.util.List;

public record Output(
        String nome,
        String cpf,
        String funcao,
        @JsonFormat(pattern = "dd/MM/yyyy")
        LocalDate dataAdmissao,
        Double remuneracao,
        List<Long> idSubordinados) {

    public String getRemuneracao() {
        return String.format("%.2f", remuneracao);
    }
}

