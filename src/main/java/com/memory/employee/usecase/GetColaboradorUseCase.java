package com.memory.employee.usecase;

import com.memory.employee.entity.colaborador.exception.ColaboradorNotFoundException;
import com.memory.employee.entity.colaborador.gateway.ColaboradorGateway;
import com.memory.employee.entity.colaborador.model.Colaborador;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetColaboradorUseCase {

    private final ColaboradorGateway colaboradorGateway;

    public GetColaboradorUseCase(ColaboradorGateway colaboradorGateway) {
        this.colaboradorGateway = colaboradorGateway;
    }

    public Output execute(InputString input) throws ColaboradorNotFoundException {

        var colaborador =
                colaboradorGateway.findByCpf(input.cpf());

        if (colaborador == null ) {
            throw new ColaboradorNotFoundException();
        }
        return toOutput(colaborador);
    }

    public Output execute(InputLong input) throws ColaboradorNotFoundException {

        var colaborador = colaboradorGateway.findById(input.id);

        if (colaborador == null ) {
            throw new ColaboradorNotFoundException();
        }
        return toOutput(colaborador);
    }

    public Page<Output> execute(Pageable pageable) {
        return colaboradorGateway.findAll(pageable).map(this::toOutput);
    }

    public Page<Output> execute(int ano, Pageable pageable){
        return colaboradorGateway.findByAno(ano, pageable).map(this::toOutput);
    }

    public Output execute(Long idColaborador1, Long idColaborador2) {
        return toOutput(getGerenteComum(idColaborador1, idColaborador2));
    }

    private Output toOutput(Colaborador colaborador) {
        return new Output(
                colaborador.getNome(),
                colaborador.getCpf(),
                colaborador.getFuncao(),
                colaborador.getDataAdmissao(),
                colaborador.getRemuneracao(),
                getSubordinados(colaborador.getId()));
    }

    public Colaborador getGerenteComum(Long id1, Long id2) {
        Colaborador col1 = colaboradorGateway.findById(id1);
        Colaborador col2 = colaboradorGateway.findById(id2);

        if (col1.getIdGerente() == null) {
            return col1;
        }
        if (col2.getIdGerente() == null) {
            return col2;
        }

        if (col1.getIdGerente() != col2.getIdGerente()) {
            return getGerenteComum(col1.getIdGerente(), col2.getIdGerente());
        }

        return colaboradorGateway.findById(col1.getIdGerente());
    }

    List<Long> getSubordinados(Long id) {
        return colaboradorGateway.findSubordinados(id);
    }

    public record InputString(
            String cpf
    ){}

    public record InputLong(
            Long id
    ){}

}

