package com.memory.employee.usecase;

import com.memory.employee.entity.colaborador.gateway.ColaboradorGateway;
import com.memory.employee.entity.colaborador.model.Colaborador;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class UpdateColaboradorUseCase {

    private final ColaboradorGateway colaboradorGateway;

    public UpdateColaboradorUseCase(ColaboradorGateway colaboradorGateway) {
        this.colaboradorGateway = colaboradorGateway;
    }

    public void execute(Input input, Long id) {

        colaboradorGateway.update(new Colaborador(
                input.nome,
                input.cpf,
                input.dataAdmissao,
                input.funcao,
                input.remuneracao,
                input.idGerente,
                null
            ), id);
    }

    public record Input(
            String nome,
            String cpf,
            LocalDate dataAdmissao,
            String funcao,
            Double remuneracao,
            Long idGerente

    ) {}

}
