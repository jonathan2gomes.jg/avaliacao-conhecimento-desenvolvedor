package com.memory.employee.entity.colaborador.model;

import com.memory.employee.infrastructure.colaborador.config.db.schema.ColaboradorSchema;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class Colaborador {
    private Long id;
    private String nome;
    private String cpf;
    private LocalDate dataAdmissao;
    private String funcao;
    private Double remuneracao;
    private Long idGerente;
    private List<Long> idSubordinados;

    public Colaborador() {
    }

    public Colaborador(Long id, String nome, String cpf, LocalDate dataAdmissao, String funcao, Double remuneracao, Long idGerente, List<Long> idSubordinados) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
        this.dataAdmissao = dataAdmissao;
        this.funcao = funcao;
        this.remuneracao = remuneracao;
        this.idGerente = idGerente;
        this.idSubordinados = idSubordinados;
    }

    public Colaborador(String nome, String cpf, LocalDate dataAdmissao, String funcao, Double remuneracao, Long idGerente, List<Long> idSubordinados) {
        this.nome = nome;
        this.cpf = cpf;
        this.dataAdmissao = dataAdmissao;
        this.funcao = funcao;
        this.remuneracao = remuneracao;
        this.idGerente = idGerente;
        this.idSubordinados = idSubordinados;
    }

    public Colaborador(Long idGerente){
        this.idGerente = idGerente;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataAdmissao() {
        return dataAdmissao;
    }

    public void setDataAdmissao(LocalDate dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public Double getRemuneracao() {
        return remuneracao;
    }

    public void setRemuneracao(Double remuneracao) {
        this.remuneracao = remuneracao;
    }

    public Long getIdGerente() {
        return idGerente;
    }

    public void setIdGerente(Long idGerente) {
        this.idGerente = idGerente;
    }

    public List<Long> getIdSubordinados() {
        return idSubordinados;
    }

    public void setIdSubordinados(List<Long> idSubordinados) {
        this.idSubordinados = idSubordinados;
    }

    public ColaboradorSchema toSchema(Colaborador colaborador) {

        if (Objects.isNull(colaborador.getIdGerente())) {
            return new ColaboradorSchema(
                colaborador.getNome(),
                colaborador.getCpf(),
                colaborador.getDataAdmissao(),
                colaborador.getFuncao(),
                colaborador.getRemuneracao(),
                null
            );
        }

        return new ColaboradorSchema(

                colaborador.getNome(),
                colaborador.getCpf(),
                colaborador.getDataAdmissao(),
                colaborador.getFuncao(),
                colaborador.getRemuneracao(),
                colaborador.getIdGerente()
        );
    }
}
