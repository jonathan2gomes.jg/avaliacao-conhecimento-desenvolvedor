package com.memory.employee.entity.colaborador.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ColaboradorNotFoundException extends RuntimeException{
    public ColaboradorNotFoundException() {
        super("O colaborador não foi encontrado.");
    }
}
