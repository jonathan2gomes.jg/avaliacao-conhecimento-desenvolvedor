package com.memory.employee.entity.colaborador.gateway;

import com.memory.employee.entity.colaborador.model.Colaborador;
import com.memory.employee.infrastructure.colaborador.config.db.schema.ColaboradorSchema;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ColaboradorGateway {

    void create(Colaborador colaborador);

    void update(Colaborador colaborador, Long id);

    void delete(Long id);

    Colaborador findById(Long id);

    Colaborador findByCpf(String cpf);

    Optional<ColaboradorSchema> findByCpfOptional(String cpf);

    Colaborador findByName(String name);

    List<Long> findSubordinados(Long id);

    Page<Colaborador> findAll(Pageable pageable);

    Page<Colaborador> findByAno(int dataAdmissao, Pageable pageable);

    List<Colaborador> findAll();
}
