package com.memory.employee.infrastructure.colaborador.config.db.schema;

import jakarta.persistence.*;

import java.time.LocalDate;

@Entity
public class ColaboradorSchema {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String cpf;

    @Column
    private LocalDate dataAdmissao;

    @Column
    private String funcao;

    @Column
    private Double remuneracao;

    @Column
    private Long idGerente;

    public ColaboradorSchema() {
    }

    public ColaboradorSchema(String name, String cpf, LocalDate dataAdmissao, String funcao, Double remuneracao, Long idGerente) {
        this.name = name;
        this.cpf = cpf;
        this.dataAdmissao = dataAdmissao;
        this.funcao = funcao;
        this.remuneracao = remuneracao;
        this.idGerente = idGerente;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataAdmissao() {
        return dataAdmissao;
    }

    public void setDataAdmissao(LocalDate dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public Double getRemuneracao() {
        return remuneracao;
    }

    public void setRemuneracao(Double remuneracao) {
        this.remuneracao = remuneracao;
    }

    public Long getIdGerente() {
        return idGerente;
    }

    public void setIdGerente(Long gerente) {
        this.idGerente = gerente;
    }

}
