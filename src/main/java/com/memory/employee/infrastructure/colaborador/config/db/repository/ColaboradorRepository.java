package com.memory.employee.infrastructure.colaborador.config.db.repository;

import com.memory.employee.infrastructure.colaborador.config.db.schema.ColaboradorSchema;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ColaboradorRepository extends JpaRepository<ColaboradorSchema, Long> {

    Optional<ColaboradorSchema> findByCpf(String cpf);

    @Query("SELECT c.id FROM ColaboradorSchema c WHERE c.idGerente = :id")
    List<Long> findSubordinados(@Param("id") Long id);

    Page<ColaboradorSchema> findAll(Pageable pageable);


    @Query(value = "SELECT * FROM colaborador_schema WHERE DATE_TRUNC('year', data_admissao) = DATE_TRUNC('year', to_date(:year || '-01-01', 'YYYY-MM-DD'))", nativeQuery = true)
    Page<ColaboradorSchema> findByAno(@Param("year") int year, Pageable pageable);

    @Query("SELECT c.idGerente FROM ColaboradorSchema c WHERE c.id = :id")
    void deleteById(Long id);

//    Optional<ColaboradorSchema> findByCpfOptional(String cpf);


}
