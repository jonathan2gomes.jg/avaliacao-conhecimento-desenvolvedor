package com.memory.employee.infrastructure.colaborador.controller;

import com.memory.employee.usecase.CreateColaboradorUseCase;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CreateColaboradorController {

    private final CreateColaboradorUseCase createColaboradorUseCase;

    public CreateColaboradorController(CreateColaboradorUseCase createColaboradorUseCase) {
        this.createColaboradorUseCase = createColaboradorUseCase;
    }

    @PostMapping("/colaborador")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Cadastra um Colaborador", description = "Somente o presidente pode possuir id nulo")
    public void createColaborador(@RequestBody Request request) {

        createColaboradorUseCase
                .execute(new CreateColaboradorUseCase.Input(
                        request.nome(), request.cpf(), request.dataAdmissao(), request.funcao(), request.remuneracao(), request.idGerente()));
    }
}
