package com.memory.employee.infrastructure.colaborador.controller;

import com.memory.employee.usecase.GetColaboradorUseCase;
import com.memory.employee.usecase.Output;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class GetColaboradorController {

    private final GetColaboradorUseCase getColaboradorUseCase;

    public GetColaboradorController(GetColaboradorUseCase getColaboradorUseCase) {
        this.getColaboradorUseCase = getColaboradorUseCase;
    }

    @GetMapping("/colaborador/cpf/{cpf}")
    @Operation(summary = "Busca um Colaborador pelo seu CPF", description = "Busca um Colaborador pelo seu CPF")
    public ResponseEntity<Output> getColaborador(@PathVariable String cpf) {

        return ResponseEntity.status(HttpStatus.OK).body(getColaboradorUseCase.execute(new GetColaboradorUseCase.InputString(cpf)));
    }

    @GetMapping("/colaborador/id/{id}")
    @Operation(summary = "Busca um Colaborador pelo seu id", description = "Busca um Colaborador pelo seu id")
    public ResponseEntity<Output> getColaboradorById(@PathVariable Long id) {

        return ResponseEntity.status(HttpStatus.OK).body(getColaboradorUseCase.execute(new GetColaboradorUseCase.InputLong(id)));
    }

    @GetMapping("/colaborador")
    @Operation(summary = "Retorna todos os colaboradores, com paginação", description = "Retorna todos os colaboradores, com paginação")
    public ResponseEntity<Map<String, Object>> getAllColaboradores(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "0") int size
    ) {
        try {
            List<Output> colaboradores;
            Pageable paging = PageRequest.of(page, size);

            Page<Output> pageColaborador = getColaboradorUseCase.execute(paging);

            colaboradores = pageColaborador.getContent();

            Map<String, Object> response = Map.of(
                    "Colaboradores", colaboradores,
                    "PaginaAtual", pageColaborador.getNumber(),
                    "TotalColaboradores", pageColaborador.getTotalElements(),
                    "TotalPaginas", pageColaborador.getTotalPages()
            );
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/colaborador/ano/{ano}")
    @Operation(summary = "Retorna todos os colaboradores admitidos no ano informado", description = "Retorna todos os colaboradores admitidos no ano informado")
    public ResponseEntity<Map<String, Object>> getAllColaboradoresByAno(
            @PathVariable int ano,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "0") int size
    ) {
        try {
            List<Output> colaboradores;
            Pageable paging = PageRequest.of(page, size);

            Page<Output> pageColaborador = getColaboradorUseCase.execute(ano, paging);

            colaboradores = pageColaborador.getContent();

            Map<String, Object> response = Map.of(
                    "Colaboradores", colaboradores,
                    "PaginaAtual", pageColaborador.getNumber(),
                    "TotalColaboradores", pageColaborador.getTotalElements(),
                    "TotalPaginas", pageColaborador.getTotalPages()
            );
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/colaborador/responsavel")
    @Operation(summary = "Retorna o gerente responsavel pelos colaboradores informados")
    public ResponseEntity<Output> getGerenteComum(@RequestParam  Long id, @RequestParam Long id2) {
        return ResponseEntity.ok(getColaboradorUseCase.execute(id, id2));
    }
}
