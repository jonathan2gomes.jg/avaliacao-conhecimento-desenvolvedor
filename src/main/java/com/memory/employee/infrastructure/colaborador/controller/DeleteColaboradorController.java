package com.memory.employee.infrastructure.colaborador.controller;

import com.memory.employee.usecase.DeleteColaboradorUseCase;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeleteColaboradorController {

    private final DeleteColaboradorUseCase deleteColaboradorUseCase;

    public DeleteColaboradorController(DeleteColaboradorUseCase deleteColaboradorUseCase) {
        this.deleteColaboradorUseCase = deleteColaboradorUseCase;
    }

    @DeleteMapping("/colaborador/{id}")
    @Operation(summary = "Deleta um colaborador pelo seu id")
    public void deleteColaborador(@PathVariable Long id) {
        deleteColaboradorUseCase.execute(new DeleteColaboradorUseCase.Input(id));
    }


}
