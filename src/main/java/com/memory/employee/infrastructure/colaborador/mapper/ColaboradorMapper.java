package com.memory.employee.infrastructure.colaborador.mapper;

import com.memory.employee.entity.colaborador.model.Colaborador;
import com.memory.employee.infrastructure.colaborador.config.db.schema.ColaboradorSchema;

public class ColaboradorMapper {

    public static ColaboradorSchema toColaboradorSchema(Colaborador colaborador, ColaboradorSchema colaboradorSchema) {


        colaboradorSchema.setName(colaborador.getNome());
        colaboradorSchema.setCpf(colaborador.getCpf());
        colaboradorSchema.setDataAdmissao(colaborador.getDataAdmissao());
        colaboradorSchema.setFuncao(colaborador.getFuncao());
        colaboradorSchema.setRemuneracao(colaborador.getRemuneracao());
        colaboradorSchema.setIdGerente(colaborador.getIdGerente());

        return colaboradorSchema;

    }

    public static Colaborador toColaborador(ColaboradorSchema colaboradorSchema) {

    Colaborador colaborador = new Colaborador();

        colaborador.setId(colaboradorSchema.getId());
        colaborador.setNome(colaboradorSchema.getNome());
        colaborador.setCpf(colaboradorSchema.getCpf());
        colaborador.setDataAdmissao(colaboradorSchema.getDataAdmissao());
        colaborador.setFuncao(colaboradorSchema.getFuncao());
        colaborador.setRemuneracao(colaboradorSchema.getRemuneracao());
        colaborador.setIdGerente(colaboradorSchema.getIdGerente());

        return colaborador;

    }



}
