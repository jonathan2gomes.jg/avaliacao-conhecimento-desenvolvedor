package com.memory.employee.infrastructure.colaborador.controller;

import jakarta.validation.constraints.*;
import java.time.LocalDate;

public record Request(
        @NotNull(message = "Nome não pode ser nulo")
        String nome,
        @NotNull(message = "CPF não pode ser nulo")
        @Pattern(regexp = "(^\\d{3}\\x2E\\d{3}\\x2E\\d{3}\\x2D\\d{2}$)", message = "CPF deve estar no formato 000.000.000-00")
        String cpf,
        @NotNull(message = "Data de admissão não pode ser nula")
        LocalDate dataAdmissao,
        @NotNull(message = "Função não pode ser nula")
        String funcao,
        @NotNull(message = "Remuneração não pode ser nula")
        Double remuneracao,
        Long idGerente
) {}