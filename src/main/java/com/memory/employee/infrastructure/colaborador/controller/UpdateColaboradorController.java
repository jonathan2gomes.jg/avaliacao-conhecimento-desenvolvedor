package com.memory.employee.infrastructure.colaborador.controller;


import com.memory.employee.usecase.UpdateColaboradorUseCase;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class UpdateColaboradorController {

    private final UpdateColaboradorUseCase updateColaboradorUseCase;

    public UpdateColaboradorController(UpdateColaboradorUseCase updateColaboradorUseCase) {
        this.updateColaboradorUseCase = updateColaboradorUseCase;
    }

    @PutMapping("/colaborador/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Atualiza os dados de um colaborador", description = "Atualiza os dados de um colaborador")
    public void updateColaborador(@PathVariable Long id, @Valid @RequestBody Request request) {

        updateColaboradorUseCase
                .execute(new UpdateColaboradorUseCase.Input(
                        request.nome(),
                        request.cpf(),
                        request.dataAdmissao(),
                        request.funcao(),
                        request.remuneracao(),
                        request.idGerente()
                ), id);
    }
}
