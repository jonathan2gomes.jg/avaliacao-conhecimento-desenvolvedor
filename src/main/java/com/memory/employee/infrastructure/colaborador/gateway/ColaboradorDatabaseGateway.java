package com.memory.employee.infrastructure.colaborador.gateway;

import com.memory.employee.entity.colaborador.exception.ColaboradorNotFoundException;
import com.memory.employee.entity.colaborador.gateway.ColaboradorGateway;
import com.memory.employee.entity.colaborador.model.Colaborador;
import com.memory.employee.infrastructure.colaborador.config.db.repository.ColaboradorRepository;
import com.memory.employee.infrastructure.colaborador.config.db.schema.ColaboradorSchema;
import com.memory.employee.infrastructure.colaborador.mapper.ColaboradorMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ColaboradorDatabaseGateway implements ColaboradorGateway {

    private final ColaboradorRepository colaboradorRepository;

    public ColaboradorDatabaseGateway(ColaboradorRepository colaboradorRepository){
        this.colaboradorRepository = colaboradorRepository;

    };

    @Override
    public void create(Colaborador colaborador) {
        colaboradorRepository.save(colaborador.toSchema(colaborador));

    }

    @Override
    public void update(Colaborador colaboradorIncoming, Long id) {
        ColaboradorSchema colaboradorSchema = colaboradorRepository.findById(id).orElseThrow(ColaboradorNotFoundException::new);
        colaboradorRepository.save(ColaboradorMapper.toColaboradorSchema(colaboradorIncoming, colaboradorSchema));
    }

    @Override
    public void delete(Long id) {
        colaboradorRepository.deleteById(id);
    }

    @Override
    public Colaborador findById(Long id) {
        return ColaboradorMapper.toColaborador(colaboradorRepository.findById(id).orElseThrow(ColaboradorNotFoundException::new));
    }

    @Override
    public Colaborador findByCpf(String cpf) {

        return ColaboradorMapper.toColaborador(colaboradorRepository.findByCpf(cpf).orElseThrow(ColaboradorNotFoundException::new));
    }

    @Override
    public Colaborador findByName(String name) {
        return null;
    }

    @Override
    public List<Long> findSubordinados(Long id) {
        return colaboradorRepository.findSubordinados(id);
    }

    @Override
    public Page<Colaborador> findAll(Pageable pageable) {

        return colaboradorRepository.findAll(pageable).map(ColaboradorMapper::toColaborador);
    }

    @Override
    public Page<Colaborador> findByAno(int dataAdmissao, Pageable pageable) {
        return colaboradorRepository.findByAno(dataAdmissao, pageable).map(ColaboradorMapper::toColaborador);
    }

    @Override
    public List<Colaborador> findAll() {
        return colaboradorRepository.findAll().stream().map(ColaboradorMapper::toColaborador).toList();
    }

    @Override
    public Optional<ColaboradorSchema> findByCpfOptional(String cpf) {
        return colaboradorRepository.findByCpf(cpf);
    }
}
