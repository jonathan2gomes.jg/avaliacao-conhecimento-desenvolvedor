package com.memory.employee.usecase;

import com.memory.employee.entity.colaborador.exception.ColaboradorNotFoundException;
import com.memory.employee.entity.colaborador.gateway.ColaboradorGateway;
import com.memory.employee.entity.colaborador.model.Colaborador;
import com.memory.employee.usecase.GetColaboradorUseCase.InputString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class GetColaboradorUseCaseTest {

    @Mock
    private ColaboradorGateway colaboradorGateway;

    private GetColaboradorUseCase getColaboradorUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        getColaboradorUseCase = new GetColaboradorUseCase(colaboradorGateway);
    }

    @Test
    void execute_StringInput_ValidCpf_ReturnsOutput() throws ColaboradorNotFoundException {
        // Arrange
        String cpf = "123.456.789-00";
        Colaborador colaborador = new Colaborador(1L, "John Doe", cpf, null, "Funcionario", 5000.0, null, Collections.emptyList());

        when(colaboradorGateway.findByCpf(cpf)).thenReturn(colaborador);

        // Act
        Output output = getColaboradorUseCase.execute(new InputString(cpf));

        // Assert
        assertEquals("John Doe", output.nome());
        assertEquals(cpf, output.cpf());
        assertEquals("Funcionario", output.funcao());
        assertNull(output.dataAdmissao());
        assertEquals(5000.0, output.remuneracao());
    }

    @Test
    void execute_StringInput_InvalidCpf_ThrowsColaboradorNotFoundException() throws ColaboradorNotFoundException {
        // Arrange
        String cpf = "123.456.789-98";

        when(colaboradorGateway.findByCpf(cpf)).thenReturn(null);

        // Act & Assert
        assertThrows(ColaboradorNotFoundException.class, () -> getColaboradorUseCase.execute(new InputString(cpf)));

        verify(colaboradorGateway).findByCpf(cpf);
        verifyNoMoreInteractions(colaboradorGateway);
    }

    @Test
    void execute_LongInput_ValidId_ReturnsOutput() throws ColaboradorNotFoundException {
        // Arrange
        Long id = 1L;
        Colaborador colaborador = new Colaborador(id, "John Doe", "123.456.789-00", null, "Funcionario", 5000.0, null, Collections.emptyList());

        when(colaboradorGateway.findById(id)).thenReturn(colaborador);

        // Act
        Output output = getColaboradorUseCase.execute(new GetColaboradorUseCase.InputLong(id));

        // Assert
        assertEquals("John Doe", output.nome());
        assertEquals("123.456.789-00", output.cpf());
        assertEquals("Funcionario", output.funcao());
        assertNull(output.dataAdmissao());
        assertEquals(5000.0, output.remuneracao());
    }

    @Test
    void execute_LongInput_InvalidId_ThrowsColaboradorNotFoundException() throws ColaboradorNotFoundException {
        // Arrange
        Long id = 1L;

        when(colaboradorGateway.findById(id)).thenReturn(null);

        // Act & Assert
        assertThrows(ColaboradorNotFoundException.class, () -> getColaboradorUseCase.execute(new GetColaboradorUseCase.InputLong(id)));

        verify(colaboradorGateway).findById(id);
        verifyNoMoreInteractions(colaboradorGateway);
    }

    @Test
    void execute_idColaborador_idColaborador2_ReturnGerenteResponsible() {
        // Arrange
        Long idColaborador = 2L;
        Long idColaborador2 = 3L;

        Colaborador presidente = new Colaborador(1L, "Jorinelson", "123.456.749-00", null, "Presidente", 19000.0, null, Arrays.asList(2L));
        Colaborador gerente = new Colaborador(2L, "Joabson", "123.456.789-00", null, "Gerente", 10000.0, 1L, Arrays.asList(3L, 4L));
        Colaborador subordinado1 = new Colaborador(3L, "Joaquim", "987.654.321-00", null, "Ajudante", 8000.0, 2L, null);
        Colaborador subordinado2 = new Colaborador(4L, "Janivaldo", "555.555.555-00", null, "Ajudante", 7000.0, 2L, null);

        when(colaboradorGateway.findById(2L)).thenReturn(gerente);
        when(colaboradorGateway.findById(1L)).thenReturn(presidente);
        when(colaboradorGateway.findById(3L)).thenReturn(subordinado1);
        when(colaboradorGateway.findById(4L)).thenReturn(subordinado2);

        // Act
        Output gerenteComum = getColaboradorUseCase.execute(idColaborador, idColaborador2);

        // Assert
        assertEquals("Jorinelson", gerenteComum.nome());

    }

    @Test
    void execute_IntAndPageable_ReturnsPageOfOutput() {
        // Arrange
        int ano = 2023;
        Pageable pageable = mock(Pageable.class);
        Colaborador colaborador = new Colaborador(1L, "John Doe", "123.456.789-00", null, "Funcionario", 5000.0, null, Collections.emptyList());
        Page<Colaborador> colaboradorPageable = mock(Page.class);
        when(colaboradorGateway.findByAno(ano, pageable)).thenReturn(colaboradorPageable);
        when(colaboradorPageable.map(any())).thenReturn(mock(Page.class));

        // Act
        Page<Output> result = getColaboradorUseCase.execute(ano, pageable);

        // Assert
        assertNotNull(result);

        verify(colaboradorGateway).findByAno(ano, pageable);
        verifyNoMoreInteractions(colaboradorGateway);
        verify(colaboradorPageable).map(any());
        verifyNoMoreInteractions(colaboradorPageable);
    }
}

