package com.memory.employee.usecase;

import com.memory.employee.entity.colaborador.gateway.ColaboradorGateway;
import com.memory.employee.entity.colaborador.model.Colaborador;
import com.memory.employee.usecase.CreateColaboradorUseCase.Input;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class CreateColaboradorUseCaseTest {

    @Mock
    private ColaboradorGateway colaboradorGateway;

    private CreateColaboradorUseCase createColaboradorUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        createColaboradorUseCase = new CreateColaboradorUseCase(colaboradorGateway);
    }

    @Test
    void execute_ValidInput_CreatesColaborador() {
        // Arrange
        Input input = new Input(
                "John Doe",
                "123.456.789-00",
                LocalDate.of(2023, 1, 1),
                "presidente",
                5000.0,
                null
        );

        Colaborador colaborador = new Colaborador(
                null,
                input.nome(),
                input.cpf(),
                input.dataAdmissao(),
                input.funcao(),
                input.remuneracao(),
                input.idGerente(),
                null
        );

        // Act
        createColaboradorUseCase.execute(input);

        // Assert
        verify(colaboradorGateway).findByCpfOptional(input.cpf());
    }

    @Test
    void execute_DuplicateCpf_ThrowsRuntimeException() {
        // Arrange
        Input input = new Input(
                "John Doe",
                "123.456.789-00",
                LocalDate.of(2023, 1, 1),
                "Funcionario",
                5000.0,
                1L
        );

        when(colaboradorGateway.findByCpfOptional(input.cpf())).thenReturn(null);

        // Act & Assert
        assertThrows(RuntimeException.class, () -> createColaboradorUseCase.execute(input));

        verify(colaboradorGateway).findByCpfOptional(input.cpf());
        verifyNoMoreInteractions(colaboradorGateway);
    }

    @Test
    void execute_InvalidGerenteId_ThrowsRuntimeException() {
        // Arrange
        Input input = new Input(
                "John Doe",
                "123.456.789-00",
                LocalDate.of(2023, 1, 1),
                "Funcionario",
                5000.0,
                1L
        );

        when(colaboradorGateway.findById(input.idGerente())).thenReturn(null);

        // Act & Assert
        assertThrows(RuntimeException.class, () -> createColaboradorUseCase.execute(input));

        verify(colaboradorGateway).findByCpfOptional(input.cpf());
        verify(colaboradorGateway).findById(input.idGerente());
        verifyNoMoreInteractions(colaboradorGateway);
    }

    @Test
    void execute_PresidenteAlreadyExists_ThrowsRuntimeException() {
        // Arrange
        Input input = new Input(
                "John Doe",
                "123.456.789-00",
                LocalDate.of(2023, 1, 1),
                "Presidente",
                5000.0,
                null
        );

        Colaborador presidente = new Colaborador(1L, "Presidente", "111.222.333-44", null, "Presidente", 10000.0, null, null);

        when(colaboradorGateway.findByCpfOptional(input.cpf())).thenReturn(Optional.empty());
        when(colaboradorGateway.findAll()).thenReturn(Collections.singletonList(presidente));

        // Act & Assert
        assertThrows(RuntimeException.class, () -> createColaboradorUseCase.execute(input));

        verify(colaboradorGateway).findByCpfOptional(input.cpf());
        verify(colaboradorGateway).findAll();
        verifyNoMoreInteractions(colaboradorGateway);
    }
}
