package com.memory.employee.usecase;

import com.memory.employee.entity.colaborador.gateway.ColaboradorGateway;
import com.memory.employee.entity.colaborador.model.Colaborador;
import com.memory.employee.usecase.DeleteColaboradorUseCase.Input;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.mockito.Mockito.when;

class DeleteColaboradorUseCaseTest {

    @Mock
    private ColaboradorGateway colaboradorGateway;

    private DeleteColaboradorUseCase deleteColaboradorUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        deleteColaboradorUseCase = new DeleteColaboradorUseCase(colaboradorGateway);
    }

    @Test
    void execute_DeletesColaboradorAndUpdatesSubordinados() {
        // Arrange
        Long id = 2L;
        Colaborador presidente = new Colaborador(1L, "Jorinelson", "123.456.749-00", null, "Presidente", 19000.0, null, Arrays.asList(2L));
        Colaborador gerente = new Colaborador(2L, "Joabson", "123.456.789-00", null, "Gerente", 10000.0, 1L, Arrays.asList(3L, 4L));
        Colaborador subordinado1 = new Colaborador(3L, "Joaquim", "987.654.321-00", null, "Ajudante", 8000.0, 2L, null);
        Colaborador subordinado2 = new Colaborador(4L, "Janivaldo", "555.555.555-00", null, "Ajudante", 7000.0, 2L, null);

        when(colaboradorGateway.findById(id)).thenReturn(gerente);
        when(colaboradorGateway.findById(1L)).thenReturn(presidente);
        when(colaboradorGateway.findById(3L)).thenReturn(subordinado1);
        when(colaboradorGateway.findById(4L)).thenReturn(subordinado2);

        // Act
        deleteColaboradorUseCase.execute(new Input(id));

        // Assert
        Mockito.verify(colaboradorGateway).delete(id);
        Assertions.assertEquals(1L, subordinado1.getIdGerente());
        Assertions.assertEquals(1L, subordinado2.getIdGerente());

    }
}



