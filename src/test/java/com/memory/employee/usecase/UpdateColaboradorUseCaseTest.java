package com.memory.employee.usecase;

import com.memory.employee.entity.colaborador.gateway.ColaboradorGateway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;

import static org.mockito.Mockito.*;

class UpdateColaboradorUseCaseTest {

    @Mock
    private ColaboradorGateway colaboradorGateway;

    private UpdateColaboradorUseCase updateColaboradorUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        updateColaboradorUseCase = new UpdateColaboradorUseCase(colaboradorGateway);
    }

    @Test
    void execute_UpdatesColaborador() {
        // Arrange
        Long id = 1L;
        UpdateColaboradorUseCase.Input input = new UpdateColaboradorUseCase.Input(
                "John Doe",
                "123456789",
                LocalDate.of(2022, 1, 1),
                "Manager",
                5000.0,
                2L
        );

        // Act
        updateColaboradorUseCase.execute(input, id);

        // Assert
        verify(colaboradorGateway).update(
                argThat(c -> c.getNome().equals("John Doe")
                        && c.getCpf().equals("123456789")
                        && c.getDataAdmissao().equals(LocalDate.of(2022, 1, 1))
                        && c.getFuncao().equals("Manager")
                        && c.getRemuneracao().equals(5000.0)
                        && c.getIdGerente().equals(2L)
                        && c.getId() == null
                ),
                eq(id)
        );
        verifyNoMoreInteractions(colaboradorGateway);
    }
}

